package calc;

import java.util.Stack;
import java.util.logging.Logger;
import log.LogLevel;
import org.jetbrains.annotations.NotNull;

import static calc.CalculatorChars.DIVIDE;
import static calc.CalculatorChars.LEFT_PARENTHESIS;
import static calc.CalculatorChars.MINUS;
import static calc.CalculatorChars.MULTIPLY;
import static calc.CalculatorChars.PLUS;
import static calc.CalculatorChars.RIGHT_PARENTHESIS;
import static calc.CalculatorChars.SPACE;
import static calc.CalculatorChars.SPACE_STRING;

public class PostfixConverter {
    /** */
    private static final @NotNull Logger LOGGER = Logger.getLogger(PostfixConverter.class.getName());

    /**
     * Transform expression into postfix form
     *
     * @param input imput expression
     * @return postfix form
     */
    public @NotNull String transform(@NotNull String input) {
        Stack<Character> parsingStack = new Stack<>();
        StringBuilder outputBuilder = new StringBuilder();

        String spacelessInput = input.replace(SPACE_STRING, "");
        for (int j = 0; j < spacelessInput.length(); j++) {
            char ch = spacelessInput.charAt(j);

            switch (ch) {
                case PLUS:
                case MINUS:
                    processOperation(parsingStack, outputBuilder, ch, 1);

                    outputBuilder.append(SPACE);

                    break;

                case MULTIPLY:
                case DIVIDE:
                    processOperation(parsingStack, outputBuilder, ch, 2);

                    outputBuilder.append(SPACE);

                    break;

                case LEFT_PARENTHESIS:
                    parsingStack.push(ch);

                    break;

                case RIGHT_PARENTHESIS:
                    processParenthesis(parsingStack, outputBuilder);

                    break;

                default:
                    outputBuilder.append(ch);

                    break;

            }
        }

        while (!parsingStack.isEmpty()) {
            outputBuilder.append(SPACE).append(parsingStack.pop());
        }

        String output = outputBuilder.toString();

        LOGGER.log(LogLevel.DEBUG, output);

        return output;
    }

    private void processOperation(@NotNull Stack<Character> parsingStack,
        @NotNull StringBuilder outputBuilder,
        char opThis,
        int opWeight) {

        while (!parsingStack.isEmpty()) {
            char opTop = parsingStack.pop();

            if (opTop == LEFT_PARENTHESIS) {
                parsingStack.push(LEFT_PARENTHESIS);

                break;
            }
            else {
                int currWeight;

                currWeight = opTop == PLUS || opTop == MINUS ? 1 : 2;

                if (currWeight < opWeight) {
                    parsingStack.push(opTop);

                    break;
                }
                else {
                    outputBuilder.append(SPACE).append(opTop);
                }
            }
        }

        parsingStack.push(opThis);
    }

    private void processParenthesis(@NotNull Stack<Character> parsingStack, @NotNull StringBuilder outputBuilder) {

        while (!parsingStack.isEmpty()) {
            char chx = parsingStack.pop();

            if (chx == LEFT_PARENTHESIS)
                break;

            else
                outputBuilder.append(SPACE).append(chx);
        }
    }
}
