import calc.operations.exceptions.UnknownOperationException;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class ConsoleCalculatorTest {
    ConsoleCalculator calculator;

    @Before public void setUp() {
        calculator = new ConsoleCalculator();
    }

    @Test public void testNegativeResult() throws UnknownOperationException {
        Assert.assertEquals("-201111", calculator.doCalculate("235235-436346"));
    }

    @Test public void testComplexExecute() throws UnknownOperationException {
        Assert.assertEquals("98.88888888888889", calculator.doCalculate("1.0 + 9 - 1/9 + 89"));
        Assert.assertEquals("98.88888888888889", calculator.doCalculate("1 + 9 - 1/9 + 89"));
    }

    @Test public void testLongTypeOverflow() throws UnknownOperationException {
        Assert.assertEquals(new BigDecimal("9223372039002259454").toString(),
            calculator.doCalculate(Long.MAX_VALUE + " + " + Integer.MAX_VALUE));
    }

    @Test(expected = UnknownOperationException.class) public void testWrongExpressionOverflow() throws UnknownOperationException {
        Assert.assertEquals("79.11111111111111", calculator.doCalculate("1.0 + 9 - 1//9 + 89"));
    }

    @Ignore("Known issue: configurable")
    @Test public void testDoubleTypeOverflow() throws UnknownOperationException {
        // while evaluating NumberOperation there is overflow check
        // in case of one number casted to double (more that Long.MAX_VALUE) there is no such check
        Assert.assertEquals(new BigDecimal(Double.MIN_VALUE).subtract(new BigDecimal(Double.MAX_VALUE)).toString(),
            calculator.doCalculate(Double.MIN_VALUE + " - " + Double.MAX_VALUE));
    }
}
