package calc;

import org.jetbrains.annotations.NotNull;

public interface CalculatorChars {
    // Parser constants
    char SPACE = ' ';

    char PLUS = '+';
    char MINUS ='-';
    char MULTIPLY = '*';
    char DIVIDE = '/';

    char LEFT_PARENTHESIS = '(';
    char RIGHT_PARENTHESIS = ')';

    // String value of space charachter
    @NotNull String SPACE_STRING = String.valueOf(SPACE);
}
