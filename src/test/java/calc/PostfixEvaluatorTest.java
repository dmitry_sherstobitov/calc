package calc;

import calc.operations.exceptions.UnknownOperationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostfixEvaluatorTest {
    PostfixEvaluator evaluator;

    @Before public void setUp() {
        evaluator = new PostfixEvaluator();
    }

    @Test public void testSimpleLongPlusExecute() throws UnknownOperationException {
        Assert.assertEquals("2", evaluator.evaluatePostfixExpression("1 1 +"));
    }

    @Test public void testSimpleDoublePlusExecute() throws UnknownOperationException {
        Assert.assertEquals("2.1", evaluator.evaluatePostfixExpression("1.1 1 +"));
    }

    @Test public void testSimpleLongMultiplyExecute() throws UnknownOperationException {
        Assert.assertEquals("198", evaluator.evaluatePostfixExpression("2 99 *"));
    }

    @Test public void testSimpleDoubleDivideExecute() throws UnknownOperationException {
        Assert.assertEquals("9.0", evaluator.evaluatePostfixExpression("9 1 /"));
    }

    @Test public void testComlexMixedExecute() throws UnknownOperationException {
        Assert.assertEquals("2.5", evaluator.evaluatePostfixExpression("1 2 / 2 +"));
    }
}
