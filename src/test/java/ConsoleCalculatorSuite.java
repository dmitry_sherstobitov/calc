import calc.PostfixConverterTest;
import calc.PostfixEvaluatorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({PostfixConverterTest.class,
    PostfixEvaluatorTest.class,
    ConsoleCalculatorTest.class})
public class ConsoleCalculatorSuite {
}
