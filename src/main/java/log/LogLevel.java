package log;

import java.util.logging.Level;

public class LogLevel extends Level {
    /**
     * DEBUG is a special level that can be used to turn on DEBUG logging.
     * This level is initialized to 750 (between CONFIG and INFO).
     */
    public static final Level DEBUG = new LogLevel("DEBUG", 750);

    private LogLevel(String name, int value) {
        super(name, value);
    }
}
