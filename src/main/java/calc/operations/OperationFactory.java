package calc.operations;

import calc.operations.exceptions.UnknownOperationException;
import calc.operations.types.CalculateOperation;
import calc.operations.types.NumberDivideOperation;
import calc.operations.types.NumberSubstractOperation;
import calc.operations.types.NumberMultiplyOperation;
import calc.operations.types.NumberPlusOperation;
import org.jetbrains.annotations.NotNull;

import static calc.CalculatorChars.DIVIDE;
import static calc.CalculatorChars.MINUS;
import static calc.CalculatorChars.MULTIPLY;
import static calc.CalculatorChars.PLUS;

public class OperationFactory {

    /**
     * Return operation for specified expression types
     * Long and Double types are now supported
     *
     * @param ch operation charachter
     * @param left left operand
     * @param right right operand
     * @return Operation to evaluate
     * @throws UnknownOperationException bug
     */
    public static @NotNull CalculateOperation getOperation(char ch,
        @NotNull String left,
        @NotNull String right) throws UnknownOperationException {
        switch (ch) {
            case PLUS:
                return new NumberPlusOperation(left, right);

            case MINUS:
                return new NumberSubstractOperation(left, right);

            case MULTIPLY:
                return new NumberMultiplyOperation(left, right);

            case DIVIDE:
                return new NumberDivideOperation(left, right);

            default:
                throw new UnknownOperationException("Unknown operation " + ch);

        }
    }
}
