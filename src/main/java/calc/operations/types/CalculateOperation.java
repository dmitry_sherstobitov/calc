package calc.operations.types;

import java.text.NumberFormat;
import org.jetbrains.annotations.NotNull;

public interface CalculateOperation {

    /** */
    static final @NotNull NumberFormat instance = NumberFormat.getInstance();

    /**
     * Get char that represents operation
     *
     * @return char
     */
    char getChar();

    /**
     * Evaluates operation
     *
     * @return result
     */
    @NotNull String evaluate();
}
