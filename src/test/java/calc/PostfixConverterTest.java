package calc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostfixConverterTest {
    PostfixConverter converter;

    @Before public void setUp() {
        converter = new PostfixConverter();
    }

    @Test public void testSimplePostfixLongPlusConverter() {
        Assert.assertEquals("1 1 +", converter.transform("1 + 1"));
    }

    @Test public void testTwoPostfixLongPlusConverter() {
        Assert.assertEquals("11 1 +", converter.transform("11 + 1"));
    }

    @Test public void testSimplePostfixDoublePlusConverter() {
        Assert.assertEquals("1.1 1.2 +", converter.transform("1.1 + 1.2"));
    }

    @Test public void testComplexPostfixDoublePlusConverter() {
        Assert.assertEquals(converter.transform("99.14325 + 1135235.99999"), "99.14325 1135235.99999 +");
    }

    @Test public void testSimplePostfixMixedPlusConverter() {
        Assert.assertEquals(converter.transform("1 + 1.2"), "1 1.2 +");
    }

    @Test public void testComplexPostfixMixedPlusConverter() {
        Assert.assertEquals("99 1135235.99999 +", converter.transform("99 + 1135235.99999"));
    }

    @Test public void testSimplePostfixMixedOpsConverter() {
        Assert.assertEquals("1 1.2 / 2 +", converter.transform("1 / 1.2 + 2"));
    }

    @Test public void testComplexPostfixMixedOpsConverter() {
        Assert.assertEquals("99 1135235.99999 344 * +", converter.transform("99 + 1135235.99999 * 344"));
    }

    @Test public void testSimpleParenthesis() {
        Assert.assertEquals("1 1 1 - 2 * +", converter.transform("1+(1-1)*2"));
    }
}
