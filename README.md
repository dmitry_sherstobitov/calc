# Console Calculator

## 1. What is Console Calculator?

Simple console calculator that supports +-*/ operations. Also you may use brackets. 


## 2. Requirements.

Java 8+. Following third party libraries used:
 
* junit 4.12
* jetbrains annotations 

## 3. Compile

mvn install

## 4. Run

Make sure that JAVA_HOME variable defined and put jar file near shell script.

cd bin; ./calculator.sh

To close calcualtor just write "exit"

## 5. Issues

There is some issues with type overflow and result display for large numbers