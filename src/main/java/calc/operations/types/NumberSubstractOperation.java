package calc.operations.types;

import calc.CalculatorChars;
import java.math.BigDecimal;
import java.text.ParseException;
import org.jetbrains.annotations.NotNull;

public class NumberSubstractOperation implements CalculateOperation {

    /** */
    private final @NotNull String left;

    /** */
    private final @NotNull String right;

    /** */
    public NumberSubstractOperation(@NotNull String left, @NotNull String right) {
        this.left = left;
        this.right = right;
    }

    /**
     * {@inheritDoc}
     */
    @Override public char getChar() {
        return CalculatorChars.MINUS;
    }

    /**
     * {@inheritDoc}
     */
    @Override public @NotNull String evaluate() {
        try {
            Number leftNum = instance.parse(left);
            Number rightNum = instance.parse(right);

            return leftNum instanceof Long && rightNum instanceof Long ?
                String.valueOf(Math.subtractExact(rightNum.longValue(), leftNum.longValue())) :
                String.valueOf(rightNum.doubleValue() - leftNum.doubleValue());
        }
        catch (ArithmeticException | ParseException e) {
            // todo BigInteger?
            BigDecimal leftNum = new BigDecimal(left);
            BigDecimal rightNum = new BigDecimal(right);

            return rightNum.subtract(leftNum).toString();
        }
    }
}
