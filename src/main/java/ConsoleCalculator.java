import calc.PostfixConverter;
import calc.PostfixEvaluator;
import calc.operations.exceptions.UnknownOperationException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/** Entry point */
public class ConsoleCalculator {

    /** */
    private static final @NotNull Logger LOGGER = Logger.getLogger(ConsoleCalculator.class.getName());

    /** */
    public static final @NotNull String EXIT_COMMAND = "exit";

    /** */
    public static final @NotNull String WAITING_STRING = "> ";

    /** */
    private final @NotNull PostfixConverter converter = new PostfixConverter();

    /** */
    private final @NotNull PostfixEvaluator evaluator = new PostfixEvaluator();

    /**
     * Run infinite loop with expression evaluating
     *
     * Wait exit to exit loop
     */
    public void run() {
        String input;

        while (true) {
            try {
                System.out.println(WAITING_STRING);

                Scanner scanner = new Scanner(System.in);

                input = scanner.nextLine();

                if (EXIT_COMMAND.equals(input)) {
                    System.out.println("Bye!");

                    System.exit(0);
                }

                String result = doCalculate(input);

                if (result != null) {
                    System.out.println(result);
                } else {
                    System.out.println("Unable to calculate expression. Please check correctness: " + input);
                }
            }
            catch (Exception e) {
                System.out.println("Error occurs during calculation. Please check expression correctness. " + e.getClass().getName());

                LOGGER.log(Level.WARNING, "Error occurs during calculation:", e);
            }
        }
    }

    /**
     * Evaluate input expression into result
     *
     * @param input String with expression
     * @return Number result
     * @throws UnknownOperationException bug
     */
    public @Nullable String doCalculate(@NotNull String input) throws UnknownOperationException {
        // todo variables support here

        return evaluator.evaluatePostfixExpression(converter.transform(input));
    }

    public static void main(String[] args) {
        System.out.println("****************************************");
        System.out.println("Welcome console calculator");
        System.out.println("Allowed operations: +-/*()");
        System.out.println("Print exit to leave the application");
        System.out.println("****************************************");

        ConsoleCalculator calculator = new ConsoleCalculator();

        calculator.run();
    }
}
