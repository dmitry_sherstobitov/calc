package calc;

import calc.operations.types.CalculateOperation;
import calc.operations.OperationFactory;
import calc.operations.exceptions.UnknownOperationException;

import java.util.EmptyStackException;
import org.jetbrains.annotations.NotNull;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Stack;
import java.util.logging.Logger;
import log.LogLevel;
import org.jetbrains.annotations.Nullable;

import static calc.CalculatorChars.SPACE_STRING;

public class PostfixEvaluator {

    /** */
    private static final @NotNull Logger LOGGER = Logger.getLogger(PostfixEvaluator.class.getName());

    /** */
    private static final @NotNull NumberFormat instance = NumberFormat.getInstance();

    /**
     * Evaluates postfix expression
     *
     * @param postfixExpr input expression
     * @return Number result
     * @throws UnknownOperationException bug
     */
    public @Nullable String evaluatePostfixExpression(@NotNull String postfixExpr) throws UnknownOperationException {

        Stack<String> operationStack = new Stack<>();

        String[] items = postfixExpr.split(SPACE_STRING);

        for (String item : items) {
            LOGGER.log(LogLevel.DEBUG, "Parsing symbol " + item);

            try {
                // if it's a number then push to stack
                instance.parse(item);

                operationStack.push(item);
            } catch (ParseException e) {
                if (item.isEmpty())
                    throw new UnknownOperationException("Unknown empty operation. Please check that expression is correct.");

                LOGGER.log(LogLevel.DEBUG, "Parsing operation " + item);

                String value1;
                String value2;

                try {
                    value1 = operationStack.pop();
                    value2 = operationStack.pop();
                } catch (EmptyStackException es) {
                    LOGGER.log(LogLevel.SEVERE, "Failed to execute postfix " + postfixExpr, e);

                    return null;
                }

                LOGGER.log(LogLevel.DEBUG, "Trying to create operation for " + value1 + " " + value2);

                CalculateOperation operation = OperationFactory.getOperation(item.charAt(0), value1, value2);

                LOGGER.log(LogLevel.DEBUG, "Evaluating operation for " + value1 + " " + value2);

                operationStack.push(operation.evaluate());
            }
        }

        return operationStack.pop();
    }
}
